package io.piveau.importing

import io.piveau.importing.rdf.ImportingRdfVerticle
import io.piveau.pipe.connector.PipeConnector
import io.piveau.vertx.PiveauVertxLauncher
import io.vertx.core.DeploymentOptions
import io.vertx.core.ThreadingModel
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.coAwait
import java.nio.file.FileAlreadyExistsException

class MainVerticle : CoroutineVerticle() {

    override suspend fun start() {

        if (!vertx.fileSystem().exists("tmp").coAwait()) {
            vertx.fileSystem().mkdir("tmp").coAwait()
        } else if (!vertx.fileSystem().props("tmp").coAwait().isDirectory) {
            throw FileAlreadyExistsException("tmp")
        }

        vertx.deployVerticle(ImportingRdfVerticle::class.java, DeploymentOptions().setThreadingModel(ThreadingModel.WORKER))
            .compose {
                PipeConnector.create(vertx, DeploymentOptions())
            }
            .onSuccess { it.publishTo(ImportingRdfVerticle.ADDRESS) }
            .coAwait()
    }
}

fun main(args: Array<String>) {
    PiveauVertxLauncher().dispatch(args.plus("run").plus(MainVerticle::class.java.name))
}
