package io.piveau.importing.rdf

import io.piveau.pipe.PipeContext
import io.piveau.rdf.*
import io.piveau.utils.gunzip
import io.vertx.core.Vertx
import io.vertx.core.file.OpenOptions
import io.vertx.core.http.HttpHeaders
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.codec.BodyCodec
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Resource
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.DCTerms
import org.apache.jena.vocabulary.RDF
import org.slf4j.LoggerFactory
import java.io.File
import java.util.concurrent.TimeUnit

data class Page(val page: Model, val total: Int)
data class Dataset(val dataset: Model, val dataInfo: JsonObject)

class DownloadSource(private val vertx: Vertx, private val client: WebClient, config: JsonObject) {

    private val log = LoggerFactory.getLogger(javaClass)

    private val preProcessing = config.getBoolean("PIVEAU_IMPORTING_PREPROCESSING", false)

    private val executor = vertx.createSharedWorkerExecutor("modelReader", 20, 10, TimeUnit.MINUTES)

    fun pagesFlow(address: String, pipeContext: PipeContext): Flow<Page> = flow {
        var nextLink: String = address
        val accept = pipeContext.config.getString("accept")
        val inputFormat = pipeContext.config.getString("inputFormat")
        val applyPreProcessing = pipeContext.config.getBoolean("preProcessing", preProcessing)
        val brokenHydra = pipeContext.config.getBoolean("brokenHydra", false)
        val delay = pipeContext.config.getLong("delay", 0)

        do {
            val tmpFileName: String =
                vertx.fileSystem().createTempFile("tmp", "piveau", ".tmp", null as String?).coAwait()
            log.trace("Temp file name: {}", tmpFileName)
            try {
                val stream = vertx.fileSystem().open(tmpFileName, OpenOptions().setWrite(true)).coAwait()
                log.trace("Temp file opened")

                log.debug("Next address: {}", nextLink)
                val request = client.getAbs(nextLink).`as`(BodyCodec.pipe(stream, true))
                if (accept != null) {
                    request.putHeader("Accept", accept)
                }

                val response = request.timeout(300000).send().coAwait()


                val finalFileName = if (address.endsWith(".gz") || response?.headers()?.get("Content-Type")
                        ?.contains("application/gzip") == true
                ) {
                    val targetFileName: String =
                        vertx.fileSystem().createTempFile("tmp", "piveau", ".tmp", null as String?).coAwait()
                    gunzip(tmpFileName, targetFileName)
                    targetFileName
                } else tmpFileName

                nextLink = when (response.statusCode()) {
                    in 200..299 -> {
                        val contentType = inputFormat ?: response.getHeader(HttpHeaders.CONTENT_TYPE.toString())
                        ?: "application/rdf+xml"
                        if (contentType.isRDF) {

                            val (fileName, content, finalContentType) = if (applyPreProcessing) {
                                val output = vertx.fileSystem().createTempFile("tmp", "piveau", ".tmp", null as String?)
                                    .coAwait()
                                val (_, finalContentType) = preProcess(
                                    File(finalFileName).inputStream(),
                                    File(output).outputStream(),
                                    contentType,
                                    address
                                )
                                Triple(output, File(output).inputStream(), finalContentType)
                            } else {
                                Triple("", File(finalFileName).inputStream(), contentType)
                            }

                            val page = try {
                                executor.executeBlocking<Model> { promise ->
                                    val model = content.readAllBytes().toModel(finalContentType, address)
                                    promise.complete(model)
                                }.coAwait()
                            } catch (e: Exception) {
                                throw Throwable("$nextLink: ${e.message}")
                            }

                            if (fileName.isNotBlank()) {
                                vertx.fileSystem().delete(fileName)
                            }

                            val hydraPaging = HydraPaging.findPaging(page, if (brokenHydra) address else null)

                            val next = hydraPaging.next

                            emit(Page(page, hydraPaging.total))

                            if (delay > 0) {
                                delay(delay)
                            }

                            next ?: ""

                        } else {
                            throw Throwable("$nextLink: Content-Type $contentType is not an RDF content type. Content:\n${response.bodyAsString()}")
                        }
                    }

                    else -> {
                        throw Throwable("$nextLink: ${response.statusCode()} - ${response.statusMessage()}\n${response.bodyAsString()}")
                    }
                }
                if (finalFileName != tmpFileName) {
                    vertx.fileSystem().delete(finalFileName)
                }
            } finally {
                log.debug("Deleting temp file {}", tmpFileName)
                vertx.fileSystem().delete(tmpFileName)
            }

        } while (nextLink.isNotBlank())
    }

    fun datasetsFlow(page: Page, pipeContext: PipeContext): Flow<Dataset> = flow {
        val removePrefix = pipeContext.config.getBoolean("removePrefix", false)
        val precedenceUriRef = pipeContext.config.getBoolean("precedenceUriRef", false)

        if (pipeContext.config.getBoolean("useTempFile", false)) {
            throw NotImplementedError("Using temp file is currently not supported")
        } else {
            // cleanup some idiosyncrasies
            page.page.removeAll(null, RDF.type, DCAT.dataset)

            val datasets: Set<Resource> = page.page.listResourcesWithProperty(RDF.type, DCAT.Dataset).toSet()
            val total = if (page.total > 0) page.total else datasets.size

            datasets.forEach { dataset ->

                val datasetModel = dataset.extractDatasetAsModel() ?: ModelFactory.createDefaultModel()

                dataset.identify(removePrefix, precedenceUriRef)?.let { id ->
                    if (id.isNotBlank()) {
                        val dataInfo = JsonObject()
                            .put("total", total)
                            .put("identifier", id)

                        // More idiosyncrasies
                        datasetModel.listStatements(null, RDF.type, DCAT.Catalog).toList().forEach { stmt ->
                            stmt.subject.extractCatalogueAsModel()?.let { model ->
                                datasetModel.remove(model)
                            }
                        }

                        if (!datasetModel.isEmpty) {
                            emit(Dataset(datasetModel, dataInfo))
                        }
                    } else {
                        pipeContext.log.error("Could not extract an identifier from dataset")
                    }
                } ?: run {
                    val stmt = dataset.getProperty(DCTerms.title)
                    if (stmt != null) {
                        pipeContext.log.warn("Could not extract an identifier from dataset. Fallback to normalizing title (highly unsafe).")
                        val normalizedTitle = stmt.`object`.asLiteral().string.asNormalized()
                        val dataInfo = JsonObject()
                            .put("total", total)
                            .put("identifier", normalizedTitle)

                        emit(Dataset(datasetModel, dataInfo))
                    } else {
                        pipeContext.log.error("Could not extract an identifier from dataset")
                        if (pipeContext.log.isDebugEnabled) {
                            pipeContext.log.debug(
                                datasetModel.presentAsTurtle()
                            )
                        }
                    }
                }
            }
        }
    }

}
