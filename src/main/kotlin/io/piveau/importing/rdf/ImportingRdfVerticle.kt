package io.piveau.importing.rdf

import io.opentelemetry.api.trace.SpanKind
import io.opentelemetry.api.trace.StatusCode
import io.piveau.importing.rdf.filter.PropertyFilter
import io.piveau.importing.rdf.filter.URIRefFilter
import io.piveau.pipe.PipeContext
import io.piveau.rdf.RDFMimeTypes
import io.piveau.rdf.presentAs
import io.piveau.telemetry.PiveauTelemetry
import io.piveau.telemetry.setPipeResourceAttributes
import io.vertx.config.ConfigRetriever
import io.vertx.config.ConfigRetrieverOptions
import io.vertx.config.ConfigStoreOptions
import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.WebClient
import io.vertx.kotlin.core.json.get
import io.vertx.kotlin.coroutines.CoroutineEventBusSupport
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

class ImportingRdfVerticle : CoroutineVerticle(), CoroutineEventBusSupport {

    private lateinit var downloadSource: DownloadSource

    private var pulse: Long = 0

    private val tracer = PiveauTelemetry.getTracer("piveau-consus-importer-rdf")

    override suspend fun start() {

        vertx.eventBus().coConsumer(ADDRESS) {
            handlePipe(it)
        }

        val envStoreOptions = ConfigStoreOptions()
            .setType("env")
            .setConfig(
                JsonObject().put(
                    "keys",
                    JsonArray()
                        .add("PIVEAU_IMPORTING_PREPROCESSING")
                        .add("PIVEAU_DEFAULT_PULSE")
                )
            )

        val config = ConfigRetriever.create(vertx, ConfigRetrieverOptions().addStore(envStoreOptions)).config.coAwait()
        downloadSource = DownloadSource(vertx, WebClient.create(vertx), config)
        pulse = config.getLong("PIVEAU_DEFAULT_PULSE", 15)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    private suspend fun handlePipe(message: Message<PipeContext>) = with(message.body()) {
        log.info("Import started.")

        val outputFormat = config.getString("outputFormat", RDFMimeTypes.NTRIPLES)
        val catalogue = config.getString("catalogue")

        val addresses = when (val address = config.get<Any>("address")) {
            is String -> listOf(address)
            is JsonArray -> address.map { it.toString() }.toList()
            else -> emptyList()
        }

        val identifiers = mutableListOf<String>()

        val delay = config.getLong("pulse", pulse)

        val propertyFilter = PropertyFilter(config.getJsonObject("propertyFilter", JsonObject()))
        val uriRefFilter = URIRefFilter(config.getJsonObject("datasetUriFilter", JsonObject()))

        tracer.span("handlePipe", pipe, SpanKind.SERVER) {

            addresses
                .asFlow()
                .flatMapConcat {
                    downloadSource.pagesFlow(it, this@with)
                }
                .cancellable()
                .flatMapConcat {
                    downloadSource.datasetsFlow(it, this@with)
                }
                .filter { uriRefFilter.match(it.dataset) }
                .filter { propertyFilter.match(it.dataset) }
                .onEach { delay(delay) }
                .onCompletion {
                    tracer.span("forwardDataset", kind = SpanKind.PRODUCER) {
                        when {
                            it != null -> {
                                setStatus(StatusCode.ERROR)
                                this.recordException(it)
                                setFailure(it)
                            }

                            else -> {
                                val dataInfo = JsonObject()
                                    .put("content", "identifierList")
                                    .put("catalogue", catalogue)

                                setPipeResourceAttributes(dataInfo)

                                    setResult(
                                        JsonArray(identifiers).encode(),
                                        "application/json",
                                        dataInfo
                                    ).forward()

                                    log.info("Importing finished")
                                    setRunFinished()
                                }
                            }
                        }
                    }
                    .collect { (dataset, dataInfo) ->
                        if (identifiers.contains(dataInfo.getString("identifier"))) {
                            log.warn("Duplicate dataset: {}", dataInfo.getString("identifier"))
                        }
                        identifiers.add(dataInfo.getString("identifier"))
                        dataInfo.put("counter", identifiers.size).put("catalogue", config.getString("catalogue"))

                    tracer.span("forwardDataset", dataInfo, SpanKind.PRODUCER) {
                        dataset.presentAs(outputFormat).let {
                            setResult(it, outputFormat, dataInfo).forward()
                            log.info("Data imported: {}", dataInfo)
                            log.debug("Data content: {}", it)
                        }
                    }
                }
        }

        setRunFinished()
    }

    companion object {
        const val ADDRESS: String = "io.piveau.pipe.new.importing.rdf.queue"
    }

}