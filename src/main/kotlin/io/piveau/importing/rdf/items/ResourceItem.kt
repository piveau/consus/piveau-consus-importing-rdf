package io.piveau.importing.rdf.items

import org.apache.jena.rdf.model.Resource

class ResourceItem(classType: Resource) : Item(classType) {}
