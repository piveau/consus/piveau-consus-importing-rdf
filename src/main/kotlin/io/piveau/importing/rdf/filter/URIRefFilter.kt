package io.piveau.importing.rdf.filter

import io.piveau.rdf.findDatasetAsResource
import io.vertx.core.json.JsonObject
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.Resource
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class URIRefFilter(val config: JsonObject) {

    private val log: Logger = LoggerFactory.getLogger(URIRefFilter::class.java)

    val include: Boolean = config.getBoolean("include", true)

    var regex: Regex = try {
        Regex(config.getString("regex", ""))
    } catch (e: Exception) {
        log.error("Irregular regular expression: {}", config.getString("regex"), e)
        Regex("")
    }

    fun match(dataset: Model): Boolean {
        if (regex.toString().isEmpty()) return true
        val resource = dataset.findDatasetAsResource() as Resource
        return regex.matches(resource.uri) && include
    }

}