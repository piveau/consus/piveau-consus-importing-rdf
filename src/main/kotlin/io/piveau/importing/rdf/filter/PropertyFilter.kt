package io.piveau.importing.rdf.filter

import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import org.apache.jena.rdf.model.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class PropertyFilter(val config: JsonObject) {

    private val log: Logger = LoggerFactory.getLogger(URIRefFilter::class.java)

    val include: Boolean = config.getBoolean("include", true)

    // Is this enough to check the validity?
    val property: Property? = config.getString("property")?.let {
        ModelFactory.createDefaultModel().createProperty(it)
    }

    private val allowedValues: JsonArray = config.getJsonArray("valid", JsonArray())

    var regex: Regex = try {
        Regex(config.getString("regex", ""))
    } catch (e: Exception) {
        log.error("Irregular regular expression: {}", config.getString("regex"), e)
        Regex("")
    }

    private val valid: Boolean = property != null && (regex.toString().isNotEmpty() || !allowedValues.isEmpty)

    fun match(dataset: Model): Boolean {
        if (!valid) return true

        val objects: List<String> = dataset.listObjectsOfProperty(property)
            .mapWith {
                when (it) {
                    is Resource -> it.uri
                    is Literal -> it.lexicalForm
                    else -> ""
                }
            }.toList()

        return objects.any {
            if (regex.toString().isNotEmpty()) {
                regex.matches(it)
            } else if (!allowedValues.isEmpty) {
                it in allowedValues
            } else {
                false // cannot happen (due to property valid)
            }
        } && include
    }

}
