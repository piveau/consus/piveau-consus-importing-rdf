package io.piveau.importing

import com.fasterxml.jackson.databind.ObjectMapper
import io.piveau.importing.rdf.DownloadSource
import io.piveau.importing.rdf.Page
import io.piveau.pipe.PipeContext
import io.piveau.pipe.model.*
import io.piveau.rdf.toModel
import io.vertx.core.DeploymentOptions
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpResponseExpectation
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.WebClient
import io.vertx.junit5.Checkpoint
import io.vertx.junit5.Timeout
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.util.concurrent.TimeUnit


@DisplayName("Testing the importer")
@ExtendWith(VertxExtension::class)
@ExperimentalCoroutinesApi
@FlowPreview
internal class ImportingTest {

    @BeforeEach
    fun startImporter(vertx: Vertx, testContext: VertxTestContext) {
        vertx.deployVerticle(MainVerticle(), DeploymentOptions())
            .onComplete { testContext.completeNow() }
            .onFailure(testContext::failNow)
    }

    @Test
    @DisplayName("Test a page without any filter")
    @Timeout(value = 5, timeUnit = TimeUnit.MINUTES)
    @Disabled
    fun testPage1(vertx: Vertx, testContext: VertxTestContext) = runTest {
        val checkpoint = testContext.checkpoint(2)
        val expectedResult = 87
        val content: Buffer = vertx.fileSystem().readFileBlocking("test-page-1.page")
        checkpoint.flag()
        val address = "https://data.gov.lv/dati/catalog.rdf"
        val model =
            content.bytes.toModel("application/n-triples", address)
        val page = Page(model, 1)
        val downloadSource = DownloadSource(vertx, WebClient.create(vertx), JsonObject())
        val pipe = PipeContext(
            Pipe(
                PipeHeader("939e62d5-c8be-4d7a-b3c5-5bea633546af"),
                PipeBody(listOf(Segment(SegmentHeader(null, "first segment", 1, "", false), SegmentBody())))
            )
        )

        try {
            var collectCount = 0
            downloadSource.datasetsFlow(page, pipe).onCompletion {
                println("Importing finished: $collectCount")
                if (collectCount == expectedResult)
                    checkpoint.flag()
            }
                .onEach {
                    delay(5)
                    //println(it)
                }.collect {
                    //println("collected dataset $it.")
                    collectCount++
                }
        } catch (e: Exception) {
            println("send page:" + e.message)
            testContext.failNow(e.message)
        } finally {
        }
    }

    @Test
    @DisplayName("Test a page CSV propertyFilter include... ")
    @Timeout(value = 5, timeUnit = TimeUnit.MINUTES)
    @Disabled
    fun testPageWithCsvFilterInclude(vertx: Vertx, testContext: VertxTestContext) = runTest {
        val checkpoint = testContext.checkpoint(3)
        val expectedResult = 22
        val content: Buffer = vertx.fileSystem().readFileBlocking("test-page-1.page")
        checkpoint.flag()
        val address = "https://data.gov.lv/dati/catalog.rdf"
        val model =
            content.bytes.toModel("application/n-triples", address)
        val page = Page(model, 1)
        val downloadSource = DownloadSource(vertx, WebClient.create(vertx), JsonObject())
        val mapper = ObjectMapper()
        val actualObj = mapper.readTree(
            "{\n" +
                    "            \"address\": \"https://data.gov.lv/dati/catalog.rdf\",\n" +
                    "            \"catalogue\": \"data-gov-lv\",\n" +
                    "            \"brokenHydra\": true,\n" +
                    "            \"preProcessing\": true,\n" +
                    "            \"propertyFilter\": {\n" +
                    "              \"property\": \"http://www.w3.org/ns/dcat#mediaType\",\n" +
                    "              \"valid\": [],\n" +
                    "              \"regex\": \"^text\\\\/csv\$\",\n" +
                    "              \"include\": true\n" +
                    "            }\n" +
                    "          }"
        )
        val pipe = PipeContext(
            Pipe(
                PipeHeader("939e62d5-c8be-4d7a-b3c5-5bea633546af"),
                PipeBody(
                    listOf(
                        Segment(
                            SegmentHeader(null, "first segment", 1, "", false), SegmentBody(
                                Endpoint("", "https://data.gov.lv/dati/catalog.rdf"), actualObj.deepCopy()
                            )
                        )
                    )
                )
            )
        )
        checkpoint.flag()
        try {
            var collectCount = 0
            downloadSource.datasetsFlow(page, pipe).onCompletion {
                println("Importing finished: $collectCount")
                if (collectCount == expectedResult)
                    checkpoint.flag()
            }
                .onEach {
                    delay(5)
                    //println(it)
                }.collect {
                    collectCount++
                }
        } catch (e: Exception) {
            println("send page:" + e.message)
            testContext.failNow(e.message)
        } finally {
        }
    }


    @Test
    @DisplayName("Test a page CSV propertyFilter exclude... ")
    @Timeout(value = 5, timeUnit = TimeUnit.MINUTES)
    @Disabled
    fun testPageWithCsvFilterExclude(vertx: Vertx, testContext: VertxTestContext) = runTest {
        val checkpoint = testContext.checkpoint(3)
        val expectedResult = 65
        val content: Buffer = vertx.fileSystem().readFileBlocking("test-page-1.page")
        checkpoint.flag()
        val address = "https://data.gov.lv/dati/catalog.rdf"
        val model =
            content.bytes.toModel("application/n-triples", address)
        val page = Page(model, 1)
        val downloadSource = DownloadSource(vertx, WebClient.create(vertx), JsonObject())
        val mapper = ObjectMapper()
        val actualObj = mapper.readTree(
            "{\n" +
                    "            \"address\": \"https://data.gov.lv/dati/catalog.rdf\",\n" +
                    "            \"catalogue\": \"data-gov-lv\",\n" +
                    "            \"brokenHydra\": true,\n" +
                    "            \"preProcessing\": true,\n" +
                    "            \"propertyFilter\": {\n" +
                    "              \"property\": \"http://www.w3.org/ns/dcat#mediaType\",\n" +
                    "              \"valid\": [],\n" +
                    "              \"regex\": \"^text\\\\/csv\$\",\n" +
                    "              \"include\": false\n" +
                    "            }\n" +
                    "          }"
        )
        val pipe = PipeContext(
            Pipe(
                PipeHeader("939e62d5-c8be-4d7a-b3c5-5bea633546af"),
                PipeBody(
                    listOf(
                        Segment(
                            SegmentHeader(null, "first segment", 1, "", false), SegmentBody(
                                Endpoint("", "https://data.gov.lv/dati/catalog.rdf"), actualObj.deepCopy()
                            )
                        )
                    )
                )
            )
        )
        checkpoint.flag()
        try {
            var collectCount = 0
            downloadSource.datasetsFlow(page, pipe).onCompletion {
                println("Importing finished: $collectCount")
                if (collectCount == expectedResult)
                    checkpoint.flag()
            }
                .onEach {
                    delay(5)
                    //println(it)
                }.collect {
                    collectCount++
                }
        } catch (e: Exception) {
            println("send page:" + e.message)
            testContext.failNow(e.message)
        } finally {
        }
    }

    @Test
    @DisplayName("pipe receiving no filter from https://data.gov.lv/dati/catalog.rdf")
    @Timeout(value = 5, timeUnit = TimeUnit.MINUTES)
    @Disabled
    fun sendPipeNoFilter(vertx: Vertx, testContext: VertxTestContext) {
        val checkpoint = testContext.checkpoint(2)
        sendPipe("test-pipe-no-filter.json", vertx, testContext, checkpoint)
    }

    @Test
    @DisplayName("pipe receiving with regex filter")
    @Timeout(value = 5, timeUnit = TimeUnit.MINUTES)
    @Disabled
    fun sendPipeWithRegexFilter(vertx: Vertx, testContext: VertxTestContext) {
        val checkpoint = testContext.checkpoint(2)
        sendPipe("test-pipe-regex.json", vertx, testContext, checkpoint)
    }

    @Test
    @DisplayName("pipe receiving with regex filter for mediaType property")
    @Timeout(value = 5, timeUnit = TimeUnit.MINUTES)
    @Disabled
    fun sendPipeWithRegexFilterMediaType(vertx: Vertx, testContext: VertxTestContext) {
        val checkpoint = testContext.checkpoint(2)
        sendPipe("test-pipe-regex-2.json", vertx, testContext, checkpoint)
    }

    @Test
    @DisplayName("pipe receiving with regex filter for mediaType property but exclude")
    @Timeout(value = 5, timeUnit = TimeUnit.MINUTES)
    @Disabled
    fun sendPipeWithRegexFilterExcludeMediaType(vertx: Vertx, testContext: VertxTestContext) {
        val checkpoint = testContext.checkpoint(2)
        sendPipe("test-pipe-regex-exclude.json", vertx, testContext, checkpoint)
    }

    @Test
    @DisplayName("pipe receiving mobilithek with regex filter for property identifier")
    @Timeout(value = 5, timeUnit = TimeUnit.MINUTES)
    @Disabled
    fun sendPipeWithRegexFilterMobilithek(vertx: Vertx, testContext: VertxTestContext) {
        val checkpoint = testContext.checkpoint(2)
        sendPipe("test-pipe-regex-3.json", vertx, testContext, checkpoint)
    }

    @Test
    @DisplayName("pipe receiving mobilithek with regex filter for property identifier but exclude")
    @Timeout(value = 5, timeUnit = TimeUnit.MINUTES)
    @Disabled
    fun sendPipeWithRegexFilterMobilithekExclude(vertx: Vertx, testContext: VertxTestContext) {
        val checkpoint = testContext.checkpoint(2)
        sendPipe("test-pipe-regex-3-exclude.json", vertx, testContext, checkpoint)
    }

    @Test
    @DisplayName("pipe receiving mobilithek with regex filter datasetUriFilter")
    @Timeout(value = 5, timeUnit = TimeUnit.MINUTES)
    @Disabled
    fun sendPipeWithDatasetUriFilterMobilithekExclude(vertx: Vertx, testContext: VertxTestContext) {
        val checkpoint = testContext.checkpoint(2)
        sendPipe("test-pipe-regex-datasetUriFilter.json", vertx, testContext, checkpoint)
    }

    private fun sendPipe(pipeFile: String, vertx: Vertx, testContext: VertxTestContext, checkpoint: Checkpoint) {
        vertx.fileSystem()
            .readFile(pipeFile)
            .compose { result ->
                val pipe = JsonObject(result)
                val client = WebClient.create(vertx)
                client.post(8080, "localhost", "/pipe")
                    .putHeader("content-type", "application/json")
                    .sendJsonObject(pipe)
                    .expecting(HttpResponseExpectation.SC_ACCEPTED)
            }
            .onSuccess { checkpoint.flag() }
            .onFailure(testContext::failNow)
    }

}
