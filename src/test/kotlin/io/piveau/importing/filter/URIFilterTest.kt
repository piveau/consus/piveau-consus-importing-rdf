package io.piveau.importing.filter

import io.piveau.importing.rdf.filter.URIRefFilter
import io.piveau.utils.dcatap.dsl.dataset
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.json.jsonObjectOf
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class URIFilterTest {

    val dataset = dataset("dataset-test-id") {
        title { "Just a title" lang "en" }
        description { "Just a description" lang "en" }
    }

    @Test
    fun `filter include true`() {
        val config = jsonObjectOf(
            "regex" to ".*\\/dataset-test-id",
            "include" to true
        )

        Assertions.assertTrue(URIRefFilter(config).match(dataset.model))
    }

    @Test
    fun `filter include false`() {
        val config = jsonObjectOf(
            "regex" to ".*\\/dataset-test-id",
            "include" to false
        )

        Assertions.assertFalse(URIRefFilter(config).match(dataset.model))
    }

    @Test
    fun `empty config filter`() {
        Assertions.assertTrue(URIRefFilter(JsonObject()).match(dataset.model))
    }

}