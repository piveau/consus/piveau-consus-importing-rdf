package io.piveau.importing.filter

import io.piveau.importing.rdf.filter.PropertyFilter
import io.piveau.utils.dcatap.dsl.dataset
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.json.jsonObjectOf
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class PropertyFilterTest {

    val dataset = dataset("dataset-test-id") {
        title { "Just a title" lang "en" }
        description { "Just a description" lang "en" }
    }

    @Test
    fun `filter regex include true`() {
        val config = jsonObjectOf(
            "property" to "http://purl.org/dc/terms/title",
            "regex" to "^Just.*",
            "include" to true
        )

        Assertions.assertTrue(PropertyFilter(config).match(dataset.model))
    }

    @Test
    fun `filter regex include false`() {
        val config = jsonObjectOf(
            "property" to "http://purl.org/dc/terms/title",
            "regex" to "^Just.*",
            "include" to false
        )

        Assertions.assertFalse(PropertyFilter(config).match(dataset.model))
    }

    @Test
    fun `filter values include true`() {
        val config = jsonObjectOf(
            "property" to "http://purl.org/dc/terms/title",
            "valid" to JsonArray().add("Just a title"),
            "include" to true
        )

        Assertions.assertTrue(PropertyFilter(config).match(dataset.model))
    }

    @Test
    fun `filter values include false`() {
        val config = jsonObjectOf(
            "property" to "http://purl.org/dc/terms/title",
            "valid" to JsonArray().add("Just a title"),
            "include" to false
        )

        Assertions.assertFalse(PropertyFilter(config).match(dataset.model))
    }

    @Test
    fun `empty config filter`() {
        Assertions.assertTrue(PropertyFilter(JsonObject()).match(dataset.model))
    }

}