# Pipe Segment Configuration

## _mandatory_

* `address`

  Address of the source
  
  It can be a single string or an array of strings

* `catalogue`

  The id of the target catalogue

## _optional_

* `pulse`

  Add a delay in milliseconds (ms) to how frequent each dataset get emitted. Default is 15ms.

* `delay`

  Add a delay in milliseconds (ms) to how frequent the next page of a multi-page result will be accessed. Default is 0ms..

* `removePrefix`

  Try to remove prefix from uriRefs and take everything after last `/` as identifier. Default is `false`.

* `precedenceUriRef`

  Give uriRef precedence over `dct:identifier` as identifier. Default is `false`.

* `inputFormat`

  Mimetype to read from source. Takes precedence over header `Content-Type`

* `outputFormat`

  Mimetype to use for payload. Default is `application/n-triples`

  Possible output formats are:

    * `application/rdf+xml`
    * `application/n-triples`
    * `application/ld+json`
    * `application/trig`
    * `text/turtle`
    * `text/n3`

* `brokenHydra`

  Some sources use a wrong urls in hydra information for paging. If set to true the service will try to handle such broken hydra information. Default is `false`

* `sendListDelay`

  The delay in milliseconds before the list of identifiers is send. Take precedence over service configuration (see `PVEAU_IMPORTING_SEND_LIST_DELAY`)

* `preProcessing`

  Any pre-processing (e.g. URI encoding fixes) should take place. Overwrites importers configuration (see `PIVEAU_IMPORTING_PREPROCESSING`)


* `datasetUriFilter` (_Deprecated_, please switch to upcoming _triples filter_ when available)
  
  Contains an object with two fields that will be used to filter the imported datasets to only allow matching URI.
  See please example in  [test-pipe-regex-datasetUriFilter.json](src\test\resources\test-pipe-regex-datasetUriFilter.json)
  
  The fields are:
  * `regex`: regex string
  * `include`: true | false depending on if the matching dataset should be included in the harvesting


* `propertyFilter` (_Deprecated_, please switch to upcoming _triples filter_ when available)

  Contains an object with two fields that will be used to filter the imported datasets to only allow matching ones.
  See please example in test-pipe-regex-2.json [test-pipe-regex-2.json](src\test\resources\test-pipe-regex-2.json)
  The fields are:
  * `property`: for specifying the full uri of the property that should be matched
  * `valid`: for a list of all values this property is allowed to contain. If none of these values matches, the dataset will not be imported.
  **Important:** If the dataset graph has multiple occurences of this property, a single match will make it valid.
  * `regex`: regex string
  * `include`: true | false depending on if the matching dataset should be included in the harvesting


* `triplesFilter` _Draft_ (not yet implemented, stay tuned)

# Data Info Object

* `total`

  Total number of datasets

* `counter`

  The number of this dataset

* `identifier`

  The unique identifier in the source of this dataset

* `catalogue`

  The id of the target catalogue
